# Nordic Easy Norge

## Installation
  
1. Install docker.
2. Additional step for Windows: Install docker runtime.
3. Execute: cp docker-compose.yml.dev docker-compose.yml.
4. Execute: docker-compose build --no-cache.
5. Execute: docker-compose exec web bash composer install.
6. Execute: docker-compose up.

## Working with project
1. Execute docker-compose up.

Site should be available on localhost:8000.  
It may be adjusted in docker-compose.yml.  
