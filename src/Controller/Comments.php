<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Uid\Uuid;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use App\Form\ClientType;
use App\Form\CommentType;
use App\Entity\Client;
use App\Entity\Comment;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/api/comments")
 */
class Comments extends AbstractController
{
    /**
     * @Rest\Post("/add")
     * 
     * @param Request $request
     */
    public function add(Request $request)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $data = $request->get('data');
        $client = $serializer->deserialize($data['client'], Client::class, 'json');
        $clientForm = $this->createForm(ClientType::class, new Client());
        $clientForm->submit($client);
        if ($clientForm->isSubmitted() && $clientForm->isValid()) {
            $comment = $serializer->deserialize($data['comment'], Comment::class, 'json');
            $commentForm = $this->createForm(CommentType::class, new Comment());
            $commentForm->submit($comment);
            if ($commentForm->isSubmitted() $commentForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->persist($comment);
                $em->flush();
                return new JsonResponse(Response::HTTP_OK);
            }
            return new JsonResponse(Response::HTTP_BAD_REQUEST);
        }
        return new JsonResponse(Response::HTTP_BAD_REQUEST);
    }
}

