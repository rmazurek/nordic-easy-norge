<?php 

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\CommentRepository;
use App\Entity\Client;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Comment
{
    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="comments")
     */
    protected $client;
    
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=1000)
     */
    protected $comment;
    
    /**
     * Set client
     * 
     * @param Client $client
     * 
     * @return void
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }
    
    /**
     * Get client
     * 
     * @return \App\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set comment
     * 
     * @param string $comment
     * 
     * @return void
     */
    public function setComment(string $comment)
    {
        $this->comment = $comment;
    }
    
    /**
     * Get comment
     * 
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
}
