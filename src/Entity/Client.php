<?php 

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Repository\ClientRepository;
use App\Entity\Comment;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Uid\Uuid;


/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="client")
     */
    protected $comments;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;
    
    // I tried to make it primary key but I'm not sure if it is a good idea and more research is needed.
    // I stick with traditional convention: all primary keys are named "id".
    /**
     * @ORM\Column(type=UuidType::NAME, unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="doctrine.uuid_generator")
     * @Assert\Uuid()
     */
    protected $clientId;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\Email()
     */
    protected $email;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phoneNumber;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    protected $name;
    
    /**
     * Class constructor
     * 
     * @return void
     */
    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }
    
    /**
     * Add comment
     * 
     * @param Comment $comment
     * 
     * @return void
     */
    public function addComment(Comment $comment)
    {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
        }
    }
    
    /**
     * Remove comment
     * 
     * @param Comment $comment
     * 
     * @return void
     */
    public function removeComment(Comment $comment)
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
        }
    }
    
    /**
     * Get comments
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }
    
    /**
     * Get id
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get client id
     * 
     * @return Uuid
     */
    public function getClientId()
    {
        return $this->clientId;
    }
    
    /**
     * Set email
     * 
     * @param string $email
     * 
     * @return void
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }
    
    /**
     * Get email
     * 
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * set phone number
     * 
     * @param string $phoneNumber
     * 
     * @return void
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }
    
    /**
     * Get phone number
     * 
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }
    
    /**
     * Set name
     * 
     * @param string $name
     * 
     * @return void
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    
    /**
     * Get name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
