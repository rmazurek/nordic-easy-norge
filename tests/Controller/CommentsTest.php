<?php 

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Client;
use App\Entity\Comment;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CommentsTest extends WebTestCase
{
    
    public function testAdd(): void
    {
        
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        
        $client = new Client();
        $client->setName('test name');
        $client->setEmail('test@test.pl');
        $client->setPhoneNumber('123123123');

        $comment = new Comment();
        $comment->setClient($client);
        $comment->setComment('test comment');

        
        $client = $serializer->serialize($client, 'json');
        $comment = $serializer->serialize($comment, 'json');
        


        $response = static::createClient()->request('POST','/api/comments/add',['data' => ['client' => $client, 'comment' => $comment]]);
        
        
        
        $this->assertResponseIsSuccessful();
        
    }
    
    
    
}
